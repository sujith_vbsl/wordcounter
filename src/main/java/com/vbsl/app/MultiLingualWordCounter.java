package com.vbsl.app;

import com.external.util.Translator;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by sujith on 08/08/2018.
 */
public class MultiLingualWordCounter extends SimpleWordCounter {

    public void add(String... words) throws WordNotAllowedException {
        try {
        wordsMap = Arrays.asList(words)
                .parallelStream()
                .filter(x -> {
                    if (!Pattern.compile("^[a-zA-Z]+$").asPredicate().test(x)) {
                        throw new IllegalArgumentException();
                    }
                    return true;
                })
                .collect(Collectors.toConcurrentMap(word -> Translator.translate(word), w -> 1, Integer::sum));
        } catch (IllegalArgumentException ilae){
            throw new WordNotAllowedException(ilae.getMessage());
        }
    }

}
