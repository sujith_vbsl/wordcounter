package com.vbsl.app;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by sujith on 08/08/2018.
 */
public class SimpleWordCounter implements WordCounter {

    Map<String, Integer> wordsMap = new Hashtable<>();

    @Override
    public void add(String... words) throws WordNotAllowedException{
        try {
            wordsMap = Arrays.asList(words)
                    .parallelStream()
                    .filter(x -> {
                        if (!Pattern.compile("^[a-zA-Z]+$").asPredicate().test(x)) {
                            throw new IllegalArgumentException();
                        }
                        return true;
                    })
                    .collect(Collectors.toConcurrentMap(word -> word, w -> 1, Integer::sum));
        } catch (IllegalArgumentException iae){
            throw new WordNotAllowedException("Words with non alphabet chars not allowed :" + iae.getMessage());
        }
    }

    @Override
    public int count(String word) throws WordNotFoundException{
        Integer count;
        try{
            count = wordsMap
                    .entrySet()
                    .stream()
                    .filter( p -> p.getKey().equalsIgnoreCase(word))
                    .map(Map.Entry::getValue)
                    .findFirst()
                    .get();

        } catch (NoSuchElementException nsee){
            throw new WordNotFoundException("Word not fount : "+nsee.getMessage());

        }
        return count;
    }
}
