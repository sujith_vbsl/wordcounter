package com.vbsl.app;

/**
 * Created by sujith on 08/08/2018.
 */
public class WordNotAllowedException extends Throwable {
    public WordNotAllowedException(String word) {
        super("Word with non alphabet characters is not allowed: " + word);

    }
}
