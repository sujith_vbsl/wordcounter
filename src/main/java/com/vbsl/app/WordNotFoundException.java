package com.vbsl.app;

/**
 * Created by sujith on 08/08/2018.
 */
public class WordNotFoundException extends Throwable {
    public WordNotFoundException(String s) {
        super("Word not found in the words map: "+ s);

    }
}
