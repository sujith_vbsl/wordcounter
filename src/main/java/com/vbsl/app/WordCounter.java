package com.vbsl.app;

/**
 * Created by sujith on 08/08/2018.
 */
public interface WordCounter {

    /**
     * a method to allow add words to a storage
     *
     * @param words
     */
    void add(String... words) throws WordNotAllowedException;

    /**
     * a method to return count the number of times a word is added to the storage
     *
     * @param words
     * @return
     */
    int count(String words) throws WordNotFoundException;
}
