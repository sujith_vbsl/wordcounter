package com.vbsl.app;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by sujith on 08/08/2018.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "com.vbsl.app.*")
public class SimpleWordCounterTest {

    private WordCounter wordCounter;

    @Before
    public void setUp() throws Exception {
        wordCounter = new SimpleWordCounter();

    }

    @Test
    public void add_one_word() throws WordNotFoundException, WordNotAllowedException {
        wordCounter.add("Hello");
        assertNotNull(wordCounter);
        assertEquals(1, wordCounter.count("Hello"));
    }

    @Test
    public void count_words() throws WordNotFoundException, WordNotAllowedException {
        wordCounter.add("Hello", "World", "World");
        assertEquals(1, wordCounter.count("Hello"));
        assertEquals(2, wordCounter.count("World"));
    }

    @Test(expected = WordNotFoundException.class)
    public void count_words_throws_word_notFound_exception() throws WordNotFoundException, WordNotAllowedException {
        wordCounter.add("Hello", "World");
        assertEquals(1, wordCounter.count("Flower"));
    }

    @Test(expected = WordNotAllowedException.class)
    public void should_not_add_word_containing_non_alphabets() throws WordNotFoundException, WordNotAllowedException {
        wordCounter.add("Hello123");
        assertEquals(0, wordCounter.count("Hello123"));
    }

    @Test(expected = WordNotAllowedException.class)
    public void should_add_multiple_words_with_only_alphabets() throws WordNotAllowedException {
        String[] words = {"Hello", "World", "123", "Asterisk*", "Hello123", "Space ", "Hello", "World"};
        wordCounter.add(words);
    }


}

