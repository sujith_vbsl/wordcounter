package com.vbsl.app;

import com.external.util.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by sujith on 08/08/2018.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = {"com.vbsl.app.*", "com.external.util.*"})
public class MultiLingualWordCounterTest {
    private WordCounter wordCounter;


    @Before
    public void setUp() throws Exception, WordNotAllowedException, WordNotFoundException {
        wordCounter = new MultiLingualWordCounter();
    }

    @Test
    public void should_add_multilingual_words_which_means_flower() throws WordNotFoundException, WordNotAllowedException {
        wordCounter = new MultiLingualWordCounter();
        mockStatic(Translator.class);

        PowerMockito.when(Translator.translate("flower")).thenReturn("flower");
        PowerMockito.when(Translator.translate("flor")).thenReturn("flower");
        PowerMockito.when(Translator.translate("blume")).thenReturn("flower");
        PowerMockito.when(Translator.translate("pushpam")).thenReturn("flower");
        wordCounter.add("flower", "flor", "blume", "pushpam");
        assertEquals(4, wordCounter.count("flower"));
        PowerMockito.verifyStatic(VerificationModeFactory.times(4));
    }

    @Test(expected = WordNotAllowedException.class)
    public void should_add_multilingual_words_which_has_non_alphabet() throws WordNotFoundException, WordNotAllowedException {
        wordCounter = new MultiLingualWordCounter();
        mockStatic(Translator.class);

        PowerMockito.when(Translator.translate("flor")).thenReturn("flower");
        PowerMockito.when(Translator.translate("blume")).thenReturn("flower");
        PowerMockito.when(Translator.translate("pushpam123")).thenReturn("flower");
        wordCounter.add("flor", "blume", "pushpam123");
        assertEquals(4, wordCounter.count("flower"));
        PowerMockito.verifyStatic(VerificationModeFactory.times(4));
    }



}
